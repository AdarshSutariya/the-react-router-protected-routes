import {Route, Navigate} from "react-router-dom";
import {FC, useEffect, useState} from "react";
import Loader from "./Loader";
import ForbiddenAccess from "./ForbiddenAccess";
import * as React from "react";
import fetchLoggedInUserDetails from "../service/user-service";
import UserService from "../service/user-service";
import Home from "./Home";

import {useContext} from "react"
import {AppContext} from "../context/Context.js"

// role is all allowed roles of user, for which this path can be rendered
// if no role is passed, then this route can be rendered for all the roles



interface  IProps {
    role?: string[];  //admin
    element: JSX.Element
}

const Auth: FC<IProps> = ({role = [], element}) => {
    const [loggedInUser, setLoggedInUser] = useState<any>({});
    const [spinning, setSpinning] = useState<any>(true);
    const [userRole, setUserRole] = useContext(AppContext);

    useEffect(() => {
        UserService.fetchLoggedInUserDetails()
        .then((response)=>{
            setLoggedInUser(response); 
            if (response && response.role) {
                setUserRole(response.role)
            } 
        })
        .catch((e)=>{
            console.error(e);
            return <ForbiddenAccess/>;
        })
        .finally(()=>{
            setSpinning(false);
        });
    
    } ,[])
    
    if (spinning) return <Loader/>


    if (role.length == 0 || role == null){
       return element;
    } 
    if (role.includes(loggedInUser.role) ) {

        return element; 
    }
    if (!(role.includes(loggedInUser.role))) {
        console.log("roles are not same.")
        return <ForbiddenAccess/>; 
    }
    
    return <Navigate to="/login" replace={true}/>
}

export default Auth;