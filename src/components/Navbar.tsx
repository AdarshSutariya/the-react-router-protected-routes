import {NavLink} from "react-router-dom";
import {AppContext} from "../context/Context.js"
import {useContext} from "react"

const Navbar = () => {
    const [userRole, setUserRole] = useContext(AppContext);
    // console.log("Role: ",userRole);

    return (
        <ul>
            <li><NavLink to="/">Main Page</NavLink></li>
            {userRole === "admin" ? <li><NavLink to="/dashboard">Dashboard Page</NavLink></li> : ""}
            <li><NavLink to="/home">Home Page</NavLink></li>
            <li><NavLink to="/login">Login Page</NavLink></li>
        </ul>
    )
}

export default Navbar;