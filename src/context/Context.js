import React, { createContext, useState } from 'react';

export const AppContext = createContext();

const { Provider } = AppContext;

export const AppProvider = (props) => {

  const [userRole, setUserRole] = useState();

return(

   <Provider value={[userRole, setUserRole]}>

      {props.children}

   </Provider>

 );

}