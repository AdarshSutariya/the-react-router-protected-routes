import React from 'react';
import './App.css';
import RouteDemo from "./components/RouteDemo";
import {AppProvider} from "./context/Context.js"

function App() {
  return (
    <div className="App">
      <AppProvider>
        <RouteDemo />
      </AppProvider>
    </div>
  );
}

export default App;
